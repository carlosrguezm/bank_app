package com.carlosrguez.repository.models

import com.google.gson.annotations.SerializedName
import java.time.LocalDate

/**
 * Transactions model
 * @property amount Double
 * @property strDate String
 * @property description String
 * @property fee Double
 * @property id Int
 * @constructor
 */

data class TransactionDTO(
    val id: String,
    @SerializedName("date")
    var strDate: String,
    var amount: String,
    val description: String?,
    val fee: String?,
    var total: String?,
    var sortDate: LocalDate?
)





