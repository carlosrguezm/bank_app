package com.carlosrguez.repository.models


/**
 * Sealed class for get the result from backend. Similar to Single<Type> for get de data or
 * handle the server error
 */
sealed class ResultHandler<out T : Any> {
    data class Success<out T : Any>(val data: T) : ResultHandler<T>()
    data class HttpError(val code: Int? = null,val message: String? = null) : ResultHandler<Nothing>()
    data class GenericError(val message: String? = null): ResultHandler<Nothing>()
    object NetworkError: ResultHandler<Nothing>()
}
