package com.carlosrguez.repository.local.db.transactionDbRepository

import androidx.lifecycle.LiveData
import com.carlosrguez.repository.local.db.entities.TransactionDB

interface ITransactionDBRepository {

    suspend fun insert(transactionDB: TransactionDB)

    suspend fun update(transactionDB: TransactionDB)

    suspend fun delete(transactionDB: TransactionDB)

    suspend fun deleteAllTransactions()

    fun getAllTransactions(): LiveData<List<TransactionDB>>
}