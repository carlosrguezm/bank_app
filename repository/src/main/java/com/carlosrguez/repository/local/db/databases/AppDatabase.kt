package com.carlosrguez.repository.local.db.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.carlosrguez.repository.local.db.dao.TransactionDao
import com.carlosrguez.repository.local.db.entities.TransactionDB

@Database(entities = [TransactionDB::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun transactionDao() : TransactionDao
}