package com.carlosrguez.repository.local.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity for transactions. All fields are string, is for visual popouse only
 * @property id String
 * @property date String
 * @property amount String
 * @property description String
 * @property fee String
 * @property total String
 * @constructor
 */
@Entity(tableName = "transactions")
data class TransactionDB(
    @PrimaryKey(autoGenerate = true) var internal_id: Int?,
    @ColumnInfo(name = "id") var id: String,
    @ColumnInfo(name = "date") var date: String,
    @ColumnInfo(name = "amount") var amount: String,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "fee") var fee: String?,
    @ColumnInfo(name = "total") var total: String?
) {
    override fun toString(): String {
        return "$description /n $amount /n $date"
    }
}