package com.carlosrguez.repository.local.db.transactionDbRepository

import androidx.lifecycle.LiveData
import com.carlosrguez.repository.local.db.dao.TransactionDao
import com.carlosrguez.repository.local.db.entities.TransactionDB


class TransactionDBRepository(private val transactionDao: TransactionDao) :
    ITransactionDBRepository {

    override suspend fun insert(transactionDB: TransactionDB) {
        transactionDao.insert(transactionDB)
    }

    override suspend fun update(transactionDB: TransactionDB) {
        transactionDao.update(transactionDB)
    }

    override suspend fun delete(transactionDB: TransactionDB) {
        transactionDao.delete(transactionDB)
    }


    override suspend fun deleteAllTransactions() {
        transactionDao.deleteAllTransactions()
    }

    override fun getAllTransactions(): LiveData<List<TransactionDB>> = transactionDao.getAllTransactions()
}