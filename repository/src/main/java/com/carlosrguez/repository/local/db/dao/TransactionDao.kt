package com.carlosrguez.repository.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.carlosrguez.repository.local.db.entities.TransactionDB

@Dao
interface TransactionDao {

    @Insert
    suspend fun insert(transactionDB: TransactionDB)

    @Update
    suspend fun update(transactionDB: TransactionDB)

    @Delete
    suspend fun delete(transactionDB: TransactionDB)

    @Query("DELETE FROM transactions")
    suspend fun deleteAllTransactions()

    @Query("SELECT * FROM transactions ORDER BY date DESC")
    fun getAllTransactions(): LiveData<List<TransactionDB>>
}