package com.carlosrguez.repository.remote.transactionRepository


import com.carlosrguez.repository.models.ResultHandler
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

/**
 * Base class to receive the result from backend and return our sealed class ResultHandler
 * with the collection or with the backend errors.
 */
open class BaseRepository {
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): ResultHandler<T> {

        return try{
            val response = call.invoke()
            if (response.isSuccessful)
                ResultHandler.Success(response.body()!!)
            else {
                ResultHandler.HttpError(response.code())
            }
        }catch (throwable: Throwable){
            when (throwable) {
                is IOException -> ResultHandler.NetworkError
                is HttpException -> {
                    val code = throwable.code()
                    ResultHandler.HttpError(code)
                }
                else -> {
                    ResultHandler.GenericError(throwable.message)
                }
            }
        }
    }

}