package com.carlosrguez.repository.remote.providers

/*
This providers are oriented for dependency injection with Koin.
We use in repo module in order to create the Retrofit Object
 */

import com.carlosrguez.repository.remote.transactionRepository.ITransactionAPI
import com.carlosrguez.repository.remote.transactionRepository.TransactionRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Provider of okHttpClient
 * @return OkHttpClient
 */
fun getHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .build()
}

/**
 * Gson Provider
 * @return Gson
 */
fun provideGson(): Gson {
    return GsonBuilder()
        .setLenient()
        .serializeNulls()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        //Register the needed adapters
        // .registerTypeAdapter(QueryParametersDTO::class.java, AdapterQueryParameter())
        .create()
}

/**
 * Provider of Retrofit Client
 * @param HttpClient OkHttpClient
 * @return Retrofit
 */
fun getRetrofit(HttpClient:OkHttpClient, gson: Gson) : Retrofit = Retrofit.Builder()
    .client(HttpClient)
    .baseUrl(com.carlosrguez.repository.BuildConfig.BaseURL)
    .addConverterFactory(GsonConverterFactory.create(gson))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .build()

/**
 * Api Provider
 * @param retrofit Retrofit
 * @return ITransactionAPI
 */
fun getTransactionApi(retrofit: Retrofit): ITransactionAPI = retrofit
    .create(ITransactionAPI::class.java)

/**
 * Final Repository object Provider
 * @param retrofit ITransactionAPI
 * @return TransactionRepository
 */
fun getRepository(retrofit: ITransactionAPI): TransactionRepository =
    TransactionRepository(
        retrofit
    )