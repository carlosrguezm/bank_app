package com.carlosrguez.repository.remote.transactionRepository

import com.carlosrguez.repository.models.TransactionDTO
import retrofit2.Response
import retrofit2.http.GET


// Retrofit interface with the GET for JsonArray of transactions

interface ITransactionAPI {
    @GET("/transactions.json")
    suspend fun getTransactions(): Response<List<TransactionDTO>>

}