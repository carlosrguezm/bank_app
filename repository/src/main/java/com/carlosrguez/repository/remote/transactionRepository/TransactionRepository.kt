package com.carlosrguez.repository.remote.transactionRepository

import com.carlosrguez.repository.models.ResultHandler
import com.carlosrguez.repository.models.TransactionDTO

/**
 * Implements BaserRepository to return the ResultHandler to the Viewmodel or the domain layer
 * @property api ITransactionAPI
 * @constructor
 */
class TransactionRepository (private val api: ITransactionAPI): BaseRepository() {

    suspend fun getTransactions(): ResultHandler<List<TransactionDTO>>? {
        return safeApiCall (call = { api.getTransactions()})
    }

}