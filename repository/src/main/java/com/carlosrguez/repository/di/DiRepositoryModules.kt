package com.carlosrguez.repository.di

/**
 * Módulos para la inyección de dependencias desde Koin
 */
import androidx.room.Room
import com.carlosrguez.repository.local.db.databases.AppDatabase
import com.carlosrguez.repository.local.db.transactionDbRepository.ITransactionDBRepository
import com.carlosrguez.repository.local.db.transactionDbRepository.TransactionDBRepository
import com.carlosrguez.repository.remote.providers.*
import org.koin.dsl.module

val remoteRepositoryModule = module {
    single { getHttpClient() }
    single { provideGson() }
    single {
        getRetrofit(
            get(),
            get()
        )
    }
    single { getTransactionApi(get()) }
    single { getRepository(get()) }
}

// Repository for Room
val dbRepositoryModule = module {
    single { Room.databaseBuilder(get(), AppDatabase::class.java, "transaction_database").build() }
    single { get<AppDatabase>().transactionDao() }
    single<ITransactionDBRepository> { TransactionDBRepository(get()) }
}