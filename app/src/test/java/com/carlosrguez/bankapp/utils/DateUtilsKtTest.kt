package com.carlosrguez.bankapp.utils

import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

class DateUtilsKtTest {

    lateinit var rightStringDate:String
    lateinit var wrongStringDate:String

    @Before
    fun setUp() {
         rightStringDate = "2018-07-24T18:10:10.000Z"
         wrongStringDate ="xxxx2018-07-24T18:10:10.000Z"
    }

    @After
    fun tearDown() {
    }

    @Test
    fun `Testing extension for convert String to Localdate`() {
        assertNotNull(rightStringDate.toBankLocalDate())
        assertNull(wrongStringDate.toBankLocalDate())
    }

    @Test
    fun `Testing extension for convert LocalDate to String`() {

    }
}