package com.carlosrguez.bankapp

import android.app.Application
import com.carlosrguez.bankapp.di.uiModule
import com.carlosrguez.repository.di.dbRepositoryModule
import com.carlosrguez.repository.di.remoteRepositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
/*
Class for initialize Koin modules
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App) //Context
            modules(listOf(remoteRepositoryModule,dbRepositoryModule, uiModule)) //Modules
        }
    }
}