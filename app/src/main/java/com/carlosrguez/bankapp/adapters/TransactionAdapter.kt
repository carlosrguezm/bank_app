package com.carlosrguez.bankapp.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.recyclerview.widget.RecyclerView
import com.carlosrguez.bankapp.databinding.ItemTransactionBinding
import com.carlosrguez.repository.models.TransactionDTO


/*
Adapter for fill the control with the Transaction item
 */
class TransactionAdapter(
    private var mValues: List<TransactionDTO>
) : RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

    private lateinit var binding: ItemTransactionBinding

    fun setCollection(filteredCollection: List<TransactionDTO>){
        this.mValues = filteredCollection
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Databinding for layout usement. ** Need Android Studio 3.6+
        binding =
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding.root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvDate.text = mValues[position].strDate
        if (mValues[position].description.isNullOrEmpty()) {
            holder.tvDescription.text = ""
        } else {
            holder.tvDescription.text = mValues[position].description
        }
        holder.tvAmount.text = mValues[position].amount + " €"
        mValues[position].fee?.let {
            holder.tvFee.text = it
        }.run {
            holder.tvFee.visibility = View.GONE
        }

        if (mValues[position].amount.toDouble() > 0) {
            holder.imgIn.setColorFilter(Color.GREEN)
        } else {
            holder.imgIn.setColorFilter(Color.RED)
        }

        if (position == 0){
            val layoutParams =
                holder.card.layoutParams
            layoutParams.height = WRAP_CONTENT
            layoutParams.width = MATCH_PARENT
            holder.card.layoutParams = layoutParams
            holder.title.visibility = View.VISIBLE
        }

    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val tvDate = binding.tvDate
        val tvDescription = binding.tvDescription
        val tvAmount = binding.tvAmount
        val tvFee = binding.tvFee
        var imgIn = binding.imgTransaction
        var card = binding.cardTransaction
        val title = binding.tvTitle

        override fun toString(): String {
            return super.toString() + " '" + tvDate.text + "'"
        }
    }
}