package com.carlosrguez.bankapp.viewModel

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carlosrguez.bankapp.utils.getfilterTransactions
import com.carlosrguez.repository.local.db.entities.TransactionDB
import com.carlosrguez.repository.local.db.transactionDbRepository.ITransactionDBRepository
import com.carlosrguez.repository.models.ResultHandler
import com.carlosrguez.repository.models.TransactionDTO
import com.carlosrguez.repository.remote.transactionRepository.TransactionRepository
import kotlinx.coroutines.launch

class TransactionViewModel(
    private val remoteRepository: TransactionRepository,
    private val dbRepository: ITransactionDBRepository
) : ViewModel(), ITransactionViewModel {

    // Livedata with the boolean for show the load spinner
    private var isloading = MutableLiveData<Boolean>()

    // Livedata with the backend errors (show in activity with a Toast)
    private var showerror = MutableLiveData<String>()

    // Livedata with transaction collections. All information from backend
    private val transactionsLiveData = MutableLiveData<MutableList<TransactionDTO>>()
    // Livedata with expenses collections
    private val filterTransactionsLiveData = MutableLiveData<MutableList<TransactionDTO>>()



    // Public get
    override fun isLoading(): MutableLiveData<Boolean> {
        return isloading
    }

    // Public get
    override fun showError(): MutableLiveData<String> {
        return showerror
    }

    fun showExpensesCollection(){
        filterTransactionsLiveData.value = transactionsLiveData.value?.filter { t -> t.amount.toDouble()<0 } as MutableList<TransactionDTO>?
    }

    fun showIncomesCollection(){
        filterTransactionsLiveData.value = transactionsLiveData.value?.filter { t -> t.amount.toDouble()>0 } as MutableList<TransactionDTO>?
    }

    /**
     * // Process the errors and then. notify it to the View
     * @param resultHandler ResultHandler<T>?
     */
    override fun <T : Any> setShowError(resultHandler: ResultHandler<T>?) {
        when (resultHandler) {
            is ResultHandler.HttpError -> {
                showerror.postValue("${resultHandler.code}")
            }
            is ResultHandler.NetworkError -> {
                showerror.postValue("Network error")
            }
            is ResultHandler.GenericError -> {
                showerror.postValue(resultHandler.message)
            }
            else -> {
                showerror.postValue("Network error")
            }
        }
    }

    // Public get for collection observer on Activity
    override fun getTransactionsLiveData(): MutableLiveData<MutableList<TransactionDTO>> {
        return transactionsLiveData
    }
    // Filtered list
    override fun getFilterTransactionsLiveData(): MutableLiveData<MutableList<TransactionDTO>> {
        return filterTransactionsLiveData
    }

    // Get the transaction for repository
    /*
    Funtional structure - MVVM Pattern:
    [Activity] <-> Livedata <-> [ViewModel] <-> Coroutines <-> [Repository]
    Usually works with a domain layer between Viewmodel and repository
    but is omitted for easy understanding of the app.
     */
    override fun fetchTransactions() {
        // Launch the viewmodel scope for run suspend function
        viewModelScope.launch {
            when (val transactions = remoteRepository.getTransactions()) {
                is ResultHandler.Success -> {
                    var list = transactions.data
                    list = list.getfilterTransactions()
                    transactionsLiveData.value = list as MutableList<TransactionDTO>?
                }
                else -> {
                    setShowError(transactions)
                }
            }
            isloading.postValue(false)
        }
    }

    /**
    Save the transactions on db for offline funtionality
    In this app will not use this collection on cases like savedInstance on activity
    Will work 100% with backend services
     */

    override fun onCleared() {
        super.onCleared()
        saveTransactions()
    }

    override fun saveTransactions() {
        try {
            transactionsLiveData.value?.let {
                viewModelScope.launch {
                    it.map {
                        dbRepository.insert(
                            TransactionDB(
                                null,
                                it.id,
                                it.strDate,
                                it.amount,
                                it.description,
                                it.fee,
                                it.total
                            )
                        )
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("ROOM_ERROR","")
        }
    }

    // Remove observes - Called when activity is destroyed to avoid memory leaks
    override fun removeObservers(lifecycleOwner: LifecycleOwner) {
        isloading.removeObservers(lifecycleOwner)
        showerror.removeObservers(lifecycleOwner)
        transactionsLiveData.removeObservers(lifecycleOwner)
    }

}