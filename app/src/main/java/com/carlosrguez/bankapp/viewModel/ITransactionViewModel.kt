package com.carlosrguez.bankapp.viewModel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.carlosrguez.repository.models.ResultHandler
import com.carlosrguez.repository.models.TransactionDTO

/**
 * Interface for our Viewmodel
 * Overrides function for communication with the activity
 * Used for run with MVVM Pattern
 */
interface ITransactionViewModel {
    fun removeObservers(lifecycleOwner: LifecycleOwner)

    fun isLoading(): MutableLiveData<Boolean>
    fun <T : Any> setShowError(resultHandler: ResultHandler<T>?)
    fun showError(): MutableLiveData<String>

    fun getTransactionsLiveData(): MutableLiveData<MutableList<TransactionDTO>>
    fun getFilterTransactionsLiveData(): MutableLiveData<MutableList<TransactionDTO>>
    fun fetchTransactions()

    fun saveTransactions()
}