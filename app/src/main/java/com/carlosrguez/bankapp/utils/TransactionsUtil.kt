package com.carlosrguez.bankapp.utils

import com.carlosrguez.repository.models.TransactionDTO

/*
kotlin extensions
Filter the collection for invalid dates and fill the total property
Finally will work in the view with our new filter collection
 */

fun List<TransactionDTO>.getfilterTransactions(): List<TransactionDTO> {
    val listVo = ArrayList<TransactionDTO>()
    this.forEach { t ->
        t.strDate.toBankLocalDate()?.let {
            t.strDate = it.toBankStringDate()
            t.sortDate = it
            if (!this.isDuplicatedId(t)) {
                val doubleOutput: Double
                t.fee?.let {
                    doubleOutput = t.amount.toDouble() + t.fee!!.toDouble()
                    t.total = doubleOutput.toString()
                }.run {
                    t.total = t.amount
                }
                listVo.add(t)
            }
        }
    }
    return listVo.sortedWith(compareByDescending { it.sortDate })
}

fun List<TransactionDTO>.isDuplicatedId(transaction: TransactionDTO): Boolean {
    if (this.count { it.id == transaction.id } < 2) return false
    else {
        this.filter { t -> t.id == transaction.id && t.sortDate != null }.forEach {
            if (it.sortDate!! > transaction.sortDate) {
                return true
            }
        }
        return false
    }
}