package com.carlosrguez.bankapp.utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Convert Localdate to String
 * @return String
 */

fun LocalDate.toBankStringDate():String{
    return try {
        val outputFormatter =
            DateTimeFormatter.ofPattern("dd MMMM", Locale.ENGLISH)
        outputFormatter.format(this)
    } catch (e: Exception) {
        ""
    }
}

/**
 * Converto from string to date
 * @return LocalDate
 */

fun String.toBankLocalDate(): LocalDate?{
    return try {
        val inputFormatter =
            DateTimeFormatter.ofPattern(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.ENGLISH
            )
        LocalDate.parse(this, inputFormatter)
    } catch (e: Exception) {
        null
    }
}