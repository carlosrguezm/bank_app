package com.carlosrguez.bankapp.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.carlosrguez.bankapp.R
import com.carlosrguez.bankapp.adapters.TransactionAdapter
import com.carlosrguez.bankapp.databinding.ActivityHomeBinding
import com.carlosrguez.bankapp.viewModel.TransactionViewModel
import kotlinx.android.synthetic.main.content_home.view.*
import org.jetbrains.anko.longToast
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Activity for the recyclerview with transactions information.
 * Receive the information with the observers of TransactionViewModel livedatas
 * @property presenter TransactionViewModel
 * @property adapter TransactionAdapter
 */
class HomeActivity : AppCompatActivity(), IBaseActivity {

    private val presenter: TransactionViewModel by viewModel()

    private lateinit var adapter: TransactionAdapter

    private lateinit var binding:ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //View binding
        binding = ActivityHomeBinding.inflate(layoutInflater)

        setContentView(binding.root)
        //Set the fix value - No backend operation
        binding.toolbar.title = "MY BANK"
        setSupportActionBar(binding.toolbar)
        // Reload transactions
        binding.fab.setOnClickListener {
            presenter.showExpensesCollection()
        }

        loadObservers()
        loadListeners()
        // Get transactions from repository
        presenter.fetchTransactions()
    }

    // Setup the livedata observers
    override fun loadObservers() {
        presenter.isLoading().observe(this, Observer {
            binding.root.swipeRefreshLayout.isRefreshing = it!!
        })

        presenter.showError().observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                binding.root.swipeRefreshLayout.isRefreshing = false
                longToast(it)
            }
        })

        presenter.getTransactionsLiveData().observe(this, Observer {
            binding.root.swipeRefreshLayout.isRefreshing = false
            binding.root.recyclerView.layoutManager = LinearLayoutManager(this)
            adapter = TransactionAdapter(it)
            binding.root.recyclerView.adapter = adapter
        })

        presenter.getFilterTransactionsLiveData().observe(this, Observer {
            binding.root.swipeRefreshLayout.isRefreshing = false
            binding.root.recyclerView.layoutManager = LinearLayoutManager(this)
            adapter = TransactionAdapter(it)
            binding.root.recyclerView.adapter = adapter
        })
    }

    // Setup listeners
    override fun loadListeners() {
        binding.root.swipeRefreshLayout.setColorSchemeColors(
            ContextCompat.getColor(this, R.color.colorPrimary),
            ContextCompat.getColor(this, R.color.white),
            ContextCompat.getColor(this, R.color.colorPrimaryDark))
        binding.root.swipeRefreshLayout.setOnRefreshListener {
            presenter.fetchTransactions()
        }
    }

    // Remove observers of livedata
    override fun removeObservers() {
        presenter.removeObservers(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObservers()
    }
}
