package com.carlosrguez.bankapp.home

/*
Interface with overrides function for communicate Activity and Viewmodel
 */
interface IBaseActivity {
    fun loadObservers()
    fun loadListeners()
    fun removeObservers()
}
