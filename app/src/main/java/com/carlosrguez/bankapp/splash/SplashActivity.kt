package com.carlosrguez.bankapp.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import com.carlosrguez.bankapp.R
import com.carlosrguez.bankapp.home.HomeActivity
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.concurrent.Executors


/*
Intro activity for check connectivity, User Validation and more background operations
 */
class SplashActivity : AppCompatActivity()  {

    private lateinit var biometricPrompt:BiometricPrompt
    private lateinit var promptInfo:BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initAuthentication()
    }

    override fun onResume() {
        super.onResume()
        //show Authentication prompt
        biometricPrompt.authenticate(promptInfo)
    }

    private fun initAuthentication(){
        val executor = Executors.newSingleThreadExecutor()

        biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                runOnUiThread {
                    toast("Error autentificando")
                }

            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                runOnUiThread {
                    startActivity<HomeActivity>()
                    finish()
                }

            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                runOnUiThread {
                    toast("biometric is valid but not recognized.")
                }
            }
        })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Identificación de usuario")
            .setSubtitle("Flower bank")
            .setDescription("Ubique su dedo en el detector de huella dactilar")
            .setNegativeButtonText("Cancelar")
            .build()
    }

}
