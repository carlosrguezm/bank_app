package com.carlosrguez.bankapp.di

import com.carlosrguez.bankapp.viewModel.TransactionViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

//UI Module with Transaction Viewmodel
val uiModule = module {
    viewModel { TransactionViewModel(get(),get()) }
}